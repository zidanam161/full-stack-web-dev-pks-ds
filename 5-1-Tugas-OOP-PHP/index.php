<!DOCTYPE html>
<html>
<body>

<?php

abstract class Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function __construct($nama, $jumlahKaki, $keahlian)
    {
      $this->nama = $nama;
      $this->jumlahKaki = $jumlahKaki;
      $this->keahlian = $keahlian;
    }

    abstract public function atraksi();
}

trait Fight {
    public $attackPower;
    public $deffencePower;

    public function setPower($attackPower, $deffencePower) {
        $this->attackPower = $attackPower;
        $this->deffencePower = $deffencePower;
    }

    public function diserang($musuh) {
        echo $this->nama .' sedang diserang '. $musuh->nama . '<br>';
        $this->darah = $this->darah - $musuh->attackPower / $this->deffencePower;
    }

    public function serang($musuh) {
        echo $this->nama .' sedang menyerang '. $musuh->nama . '<br>';
        $this->diserang($musuh);
    }
}

class Harimau extends Hewan {
    use Fight;
    public function atraksi() {
        echo $this->nama ." ". $this->keahlian ."<br>";
    }

    public function getInfoHewan() {
        echo "Nama: ". $this->nama ."<br>";
        echo "Darah: ". $this->darah ."<br>";
        echo "Jumlah Kaki: ". $this->jumlahKaki ."<br>";
        echo "Keahlian: ". $this->keahlian ."<br>";
        echo "Attack Power: ". $this->attackPower ."<br>";
        echo "Deffence Power: ". $this->deffencePower ."<br>";
    }
}

class Elang extends Hewan {
    use Fight;
    public function atraksi() {
        echo $this->nama ." ". $this->keahlian ."<br>";
    }

    public function getInfoHewan() {
        echo "Nama: ". $this->nama ."<br>";
        echo "Darah: ". $this->darah ."<br>";
        echo "Jumlah Kaki: ". $this->jumlahKaki ."<br>";
        echo "Keahlian: ". $this->keahlian ."<br>";
        echo "Attack Power: ". $this->attackPower ."<br>";
        echo "Deffence Power: ". $this->deffencePower ."<br>";
    }
}

$harimau = new Harimau("harimau galak", 4, "lari cepat");
$elang = new Elang("elang besar", 2, "terbang tinggi");

$harimau->setPower(7, 8);
$elang->setPower(10, 5);

echo "<strong>Harimau</strong><br>";
$harimau->atraksi();
$harimau->serang($elang);
$harimau->getInfoHewan();
echo "<br>";

echo "<strong>Elang</strong><br>";
$elang->atraksi();
$elang->serang($harimau);
$elang->getInfoHewan();
echo "<br>";

?>
 
</body>
</html>