export const ListPage = {
    template: `
    <div>
        <ul v-for="people of peoples">
            <li>
                <h4>Name: {{ people.name }}</h4>
                <p>Job Title: {{ people.job_title }} || 
                <router-link :to="'/detail/'+ people.id" tag="button">Detail</router-link>
                </p>
            </li>
        </ul>
    </div>
    `,
    computed: {
        peoples() {
            return this.$store.state.peoples
        }
    }
}