export const DetailPage = {
    template: `
    <div>
        <h2>{{ people.name }}</h2>
        <strong>{{ people.gender }}</strong>
        <p>Job Title: {{ people.job_title }}</p>
        <p>Language: {{ people.language }}</p>
        <p>Slogan: {{ people.slogan }}</p>
    </div>
    `,
    computed: {
      people() {
          return this.$store.getters.getData(this.$route.params.id)
      }
    }
}