var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

readBooksPromise(10000, books[0])
   .then(
       function(time1) {
        readBooksPromise(time1, books[1])
        .then(
            function(time2) {
                readBooksPromise(time2, books[2])
                .then(
                     function(time3) {
                        readBooksPromise(time3, books[3])
                        .then(
                            function (time4) {},
                            function (time4) {}
                        )
                    },
                    function(time) {}
                    ) 
                },
            function(time) {}
            )
       },
       function(time) {}
       )