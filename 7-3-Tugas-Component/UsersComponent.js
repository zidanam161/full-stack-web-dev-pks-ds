export var UsersComponent = Vue.component('users-component', {
    template: 
    `div class="usersList">
        <ul>
            <li v-for="(user, index) of users" :key="index">
                {{ user.name }} ||
                <button @click="$emit('edit', index)">Edit</button>
                <button @click="$emit('delete', index)">Delete</button>
            </li>
        </ul>
    </div>`,
    props: ['users']
})