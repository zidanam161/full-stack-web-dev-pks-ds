<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kode OTP</title>
</head>
<body>
    Selamat datang di <strong>Post 'n Comment</strong>. Ini adalah kode OTP Anda : {{ $otp->otp }}. Kode OTP ini berlaku 5 menit. Jangan berikan kode ini kepada siapapun.
</body>
</html>