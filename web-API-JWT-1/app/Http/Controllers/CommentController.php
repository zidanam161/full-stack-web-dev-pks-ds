<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Comment;
use App\Events\CommentEvent;

class CommentController extends Controller
{
    public function index()
    {
        $comments = Comment::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'List Data Comment',
            'data'    => $comments
        ], 200);
    }

    public function show($id)
    {
        $comment = Comment::findOrfail($id);

        return response()->json([
            'success' => true,
            'message' => 'Detail Data Comment',
            'data'    => $comment
        ], 200);
    }

    public function store(Request $request) 
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required',
            'post_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comment = Comment::create([
            'content' => $request->content,
            'post_id' => $request->post_id
        ]);

        event(new CommentEvent($comment));

        if ($comment) {
            return response()->json([
                'success' => true,
                'message' => 'Comment Created',
                'data'    => $comment
            ], 201);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment Failed to Save',
        ], 409);
    }

    public function update(Request $request, Comment $comment)
    {
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comment = Comment::findOrFail($comment->id);

        $user = auth()->user();

        if ($comment) {

            if ($comment->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Post is not belong to login user'
                ], 403);
            }

            $comment->update(['content' => $request->content]);

            return response()->json([
                'success' => true,
                'message' => 'Comment Updated',
                'data'    => $commnet
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }

    public function destroy($id)
    {
        $comment = Comment::findOrfail($id);

        $user = auth()->user();

        if($comment) {

            if ($comment->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Post is not belong to login user'
                ], 403);
            }

            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }
}
