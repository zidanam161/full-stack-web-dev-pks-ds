<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\OtpCode;
use Carbon\Carbon;
use App\Events\RegisterEvent;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email'  => 'required|unique:users,email|email',
            'password' => 'required|min:6',
            'username' => 'required|unique:users,username'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'username' => $request->username
        ]);

        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $random)->first();
        } while ($check);

        $now = Carbon::now();

        $otp_code = OtpCode::create([
            'otp' => $random,
            'user_id' => $user->id,
            'valid_until' => $now->addMinutes(5)
        ]);

        event(new RegisterEvent($otp_code));

        return response()->json([
            'success' => true,
            'message' => 'Data User berhasil dibuat!',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ], 200);
    }
}
