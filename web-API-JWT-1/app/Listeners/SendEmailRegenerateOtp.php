<?php

namespace App\Listeners;

use App\Events\OtpEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegenerateOtpMail;

class SendEmailRegenerateOtp implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpEvent  $event
     * @return void
     */
    public function handle(OtpEvent $event)
    {
        Mail::to($event->otp->user->email)->send(new RegenerateOtpMail($event->otp));
    }
}
