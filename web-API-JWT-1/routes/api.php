<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Auth'], function() {

    Route::post('register', 'RegisterController');

    Route::post('regenerate-otp-code', 'RegenerateOtpCodeController');

    Route::post('verification', 'VerificationController');

    Route::post('update-password', 'UpdatePasswordController');

    Route::post('login', 'LoginController');
});

Route::apiResource('post', 'PostController')->only([
    'store', 'update', 'destroy'
])->middleware('auth:api');

Route::apiResource('post', 'PostController')->except([
    'store', 'update', 'destroy'
]);

Route::apiResource('comment', 'CommentController')->only([
    'store', 'update', 'destroy'
])->middleware('auth:api');

Route::apiResource('comment', 'CommentController')->except([
    'store', 'update', 'destroy'
]);

Route::apiResource('role', 'RoleController');