/**
 * Soal Nomor 1: buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang 
 * dengan arrow function lalu gunakan let atau const di dalam soal ini
 */

//jawaban soal 1
const hitungLuas = (p, l) => p * l

const hitungKeliling = (p, l) => (p + l) * 2

console.log(hitungLuas(5, 2))

console.log(hitungKeliling(5, 2))


/**
 * Soal Nomor 2: Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana
 * 
 * const newFunction = function literal(firstName, lastName){
 *   return {
 *     firstName: firstName,
 *     lastName: lastName,
 *     fullName: function(){
 *         console.log(firstName + " " + lastName)
 *     }
 *   }
 * }
*/

//jawaban soal 2
const newFunction = (firstName, lastName) => {
    return {firstName, lastName,
        fullName: () => console.log(`${firstName} ${lastName}`)
    }
}

newFunction("William", "Imoh").fullName()

/**
 * Soal nomor 3
 * Diberikan sebuah objek sebagai berikut:
 */
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
 
//Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)

//jawaban soal 3
const {firstName, lastName, address, hobby} = newObject

console.log(firstName, lastName, address, hobby)


/**
 * Soal Nomor 4
 * Kombinasikan dua array berikut menggunakan array spreading ES6
 */
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

//jawaban soal 4
let combined = [...west, ...east]

console.log(combined)


/**
 * sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:
 */

//jawaban soal 5
const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`

console.log(before)