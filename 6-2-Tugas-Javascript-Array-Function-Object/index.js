
/**
 * Soal Nomor 1
 * buatlah variabel seperti di bawah ini
 * var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
 * 
 * urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):
 * 1. Toke
 * 2. Komodo
 * 3. Cicak
 * 4. Ular
 * 5. Buaya
 */

//jawaban Nomor 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

var hewanUrut = daftarHewan.sort()

hewanUrut.forEach(element => {
    console.log(element);
});


/**
 * Soal Nomor 2
 * Tulislah sebuah function dengan nama introduce() 
 * yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: 
 * "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!"
 */

//jawaban Nomor 2
function introduce(bio) {
    return "Nama saya "+bio.name+", umur saya "+ bio.age +" tahun, alamat saya di " + bio.address + ", dan saya punya hobby yaitu "+ bio.hobby +"!"
}
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" };
 
var perkenalan = introduce(data);
console.log(perkenalan);


/**
 * Soal Nomor 3
 * Tulislah sebuah function dengan nama hitung_huruf_vokal() 
 * yang menerima parameter sebuah string, 
 * kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.
 */

//jawaban Nomor 3
function hitung_huruf_vokal(text) {
    var vowels = ["a", "i", "u", "e", "o"];

    var jumlah = 0
    for (var i = 0; i < text.length; i++) {
        var word = text[i].toLowerCase();
        var isVokal = false

        vowels.forEach(vowel => {
            if (word == vowel) {
              isVokal = true
            }
        });
        if (isVokal) {
          jumlah++
        }
    }
   return jumlah
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)


/**
 * Soal nomor 4
 * Buatlah sebuah function dengan nama hitung() 
 * yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer,
 * dengan contoh input dan otput sebagai berikut.
 * 
 * console.log( hitung(0) ) // -2
 * console.log( hitung(1) ) // 0
 * console.log( hitung(2) ) // 2
 * console.log( hitung(3) ) // 4
 * console.log( hitung(5) ) // 8
 */

//jawaban Nomor 4
function hitung(angka) {
    var start = -2
    var tambah = angka * 2
    return start += tambah
}

console.log(hitung(0))
console.log(hitung(1))
console.log(hitung(2))
console.log(hitung(3))
console.log(hitung(5))